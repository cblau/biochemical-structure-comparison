#!/usr/bin/python3
import numpy as np
import argparse
import logging
import pandas as pd

# We need biopandas here

try:
    from biopandas.pdb import PandasPdb
except:
    print("Missing biopandas, install with " "pip3 install biopandas" "")
    exit

def get_command_line_arguments():
    """ build, parse and return command line arguments """

    parser = argparse.ArgumentParser(
        description=
        "Set chain identifiers.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    requiredNamed = parser.add_argument_group('required')

    requiredNamed.add_argument(
        "--input",
        default="input.pdb",
        help="pdb file to shift resiude numbers.",
        required=True)

    parser.add_argument("--chainchars",
                               default="ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
                               help="Characters to be sequentially used as chain ids.")

    parser.add_argument("--output",
                        default="set-chain-ids.pdb",
                        help="New pdb file containing new chain ids.")

    return parser.parse_args()

command_line_arguments = get_command_line_arguments()
pdb_file = PandasPdb().read_pdb(command_line_arguments.input)

# find the rows where residue numbering is no longer ascending by substracting
# subsequent resiude numbers from another
# ignore the NaN that occurs, because diff is not defined for the first value
residueNumberChange = pdb_file.df['ATOM'].residue_number.diff().fillna(0)

# convert diff to be zero when >=0, one when negative to indicate jump
# in residue numbering : np.heaviside(-x,0) does that
# the cumulative sum of change inidcator numbers the chains 0,1,2,...
chainIDs = residueNumberChange.transform(lambda x:
                                         np.heaviside(-x,0)).cumsum()

# change the datatype from float to integer so that this array can be used to
# index the characters provided for the chains
chainIDs = pd.to_numeric(chainIDs, downcast = 'integer')

# transform chain numbering to alphabetic description
pdb_file.df['ATOM'].chain_id = chainIDs.transform(
    lambda x: command_line_arguments.chainchars[x])

pdb_file.to_pdb(command_line_arguments.output)
