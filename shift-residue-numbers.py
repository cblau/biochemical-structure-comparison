#!/usr/bin/python3
import numpy as np
import argparse
import logging
import pandas as pd

# We need biopandas here

try:
    from biopandas.pdb import PandasPdb
except:
    print("Missing biopandas, install with " "pip3 install biopandas" "")
    exit

def get_command_line_arguments():
    """ build, parse and return command line arguments """

    parser = argparse.ArgumentParser(
        description=
        "Shift all residue numbers in a file.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    requiredNamed = parser.add_argument_group('required')

    requiredNamed.add_argument(
        "--input",
        default="input.pdb",
        help="pdb file to shift resiude numbers.",
        required=True)

    parser.add_argument("--output",
                        default="shifted-residue-numbers.pdb",
                        help="New pdb file containing shifted residue numbers.")

    parser.add_argument(
        "--add",
        default=-1,
        help="What to add to all residue numbers.")

    return parser.parse_args()

command_line_arguments = get_command_line_arguments()
pdb_file = PandasPdb().read_pdb(command_line_arguments.input)

pdb_file.df['ATOM'].residue_number = pdb_file.df['ATOM'].residue_number + int(
    command_line_arguments.add)

pdb_file.to_pdb(command_line_arguments.output)
