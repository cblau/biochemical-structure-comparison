Biochemical Structure Comparison by residue-RMSD
===

Python command line tools to aid comparison of `.pdb` structures by root mean square deviation.
