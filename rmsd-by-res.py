#!/usr/bin/python3
import numpy as np
import argparse
import logging
import pandas as pd

# We need biopandas here

try:
    from biopandas.pdb import PandasPdb
except:
    print("Missing biopandas, install with " "pip3 install biopandas" "")
    exit


def get_command_line_arguments():
    """ build, parse and return command line arguments """

    parser = argparse.ArgumentParser(
        description="Calculate the root mean square deviation between residues and save "
        "them in an output pdb file. Residues are matched on chain identifier, "
        "residue number and atom name (in  that order) and ignore hydrogen atoms. "
        "Atoms that are not matching are reported in a log file.\n\n"
        "NOTE: coordinates are not aligned.\n"
        "NOTE: missing chain ID will be replaced by A, missing atom names by X",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    requiredNamed = parser.add_argument_group('required')

    requiredNamed.add_argument(
        "--reference",
        default="reference.pdb",
        help="pdb file that provides the reference.",
        required=True)
    requiredNamed.add_argument(
        "--other",
        default="other.pdb",
        help="pdb file to compare to and template for the output pdb.",
        required=True)

    parser.add_argument("--output",
                        default="rmsd-by-res.pdb",
                        help="New pdb file containing coordinates from --other and rmsd in the b-factor column.")
    parser.add_argument("--log",
                        default="rmsd-by-res-mismatches.log",
                        help="Logfile with mismatching atoms in input pdbs.")

    return parser.parse_args()


def read_pdb_without_hydrogens(filename):
    pdb = PandasPdb().read_pdb(filename)
    return pdb.get('hydrogen', invert=True)


def merged_input_pdb_without_hydrogens(reference_filename, other_filename,
                                       rsuffix):
    """ 
    Combine the two pdbs into one, matching the identifying columns.
    Ignore non-matching atoms
    """

    reference_atoms = read_pdb_without_hydrogens(reference_filename)
    other_atoms = read_pdb_without_hydrogens(other_filename)

    identifying_columns = ['chain_id', 'residue_number', 'atom_name']

    return other_atoms.merge(reference_atoms.set_index(identifying_columns),
                             on=identifying_columns,
                             suffixes=[None, rsuffix],
                             how='outer',
                             indicator=True)


def log_differing_atoms(combined, logger, lhs_filename, rhs_filename):
    """ 
    Report mismatching atom names by printing mismatching columns in merged
    record 
    """

    # groupBy introduces a multiindex that makes column and index names ambiguous
    # rename so that sort_values does not throw an error
    combined.index.names = ['chain', 'residue', 'atomId']
    select_only_columns = combined[combined['_merge'] != 'both'].sort_values(
        ["chain_id", "residue_number", "_merge", "atom_name"])

    select_only_columns.replace(np.nan, "", inplace=True)
    if len(select_only_columns) > 0:
        logger.warning(
            "The following atoms were skipped because they had no match in the other file\n\n \t\tleft : {} \t right : {}\n"
            .format(lhs_filename, rhs_filename))
        logger.warning(select_only_columns[[
            "chain_id", "residue_number", "residue_name", "residue_name_ref",
            "atom_name"
        ]].to_string(index=False))
        logger.warning(
            "Skipped {} atoms in total due to mismatch. Have a look in the .log file for more information."
            .format(len(select_only_columns)))

    return


def setup_logging():
    logfile_handle = logging.FileHandler("rmsd-by-res.log")
    logfile_handle.setLevel(logging.INFO)

    logstream_handle = logging.StreamHandler()
    logstream_handle.setLevel(logging.WARNING)

    logging.basicConfig(level=logging.INFO,
                        format='%(message)s',
                        handlers=[logfile_handle, logstream_handle])
    return logging.getLogger('rmsd-by-res')


def rmsd(group):
    ''' Calculate the root mean square difference between atoms in group '''

    # set the b_factor field
    group['b_factor'] = np.sqrt(group['dr2_ref'].mean())

    return group


def squared_atom_distance(f, rsuffix):
    return np.square(f['x_coord'] - f['x_coord' + rsuffix]) + np.square(f['y_coord'] - f['y_coord' + rsuffix]) + np.square(f['z_coord'] - f['z_coord' + rsuffix])


def per_residue_rmsd_from_merged_pdb(pdb_data_frame, rsuffix):

    rmsd_grouping = ['chain_id', 'residue_number']
    pdb_data_frame['dr2_ref'] = squared_atom_distance(pdb_data_frame, rsuffix)

    result = pdb_data_frame.groupby(
        rmsd_grouping, group_keys=True).apply(lambda x: rmsd(x))

    # Swap atoms that are symmetric in ref see if rmsd distance is smaller
    # find all ..1 atom names
    # OT1 <-> OT2 / NH1 <-> NH2 / LEU CD1 <-> CD2 / ARG NH1 <-> NH2 ...
    symmetry_candidates = pdb_data_frame[pdb_data_frame['atom_name'].str.contains(
        '..1')][['atom_name', 'chain_id', 'residue_number']]
    swapped_pdb = pdb_data_frame.copy()
    for row in symmetry_candidates.itertuples():

        swap_atom = swapped_pdb[
            (swapped_pdb['chain_id'] == row.chain_id) &
            (swapped_pdb['residue_number'] == row.residue_number) &
            (swapped_pdb['atom_name'] == row.atom_name[:2] + '2')]
        # if there is atom number 2
        # swap atom coordinate data in ref
        if not swap_atom.empty:
            index1 = row.Index
            index2 = swap_atom.index[0]
            for d in ['x_coord', 'y_coord', 'z_coord']:
                swapped_pdb.at[index2, d], swapped_pdb.at[index1,
                                                          d] = swapped_pdb.at[index1, d], swapped_pdb.at[index2, d]

    swapped_pdb['dr2_ref'] = squared_atom_distance(swapped_pdb, rsuffix)
    swapped = swapped_pdb.groupby(
        rmsd_grouping, group_keys=True).apply(lambda x: rmsd(x))

    result['b_factor'] = np.minimum(result['b_factor'], swapped['b_factor'])

    return result


def log_results(logger, command_line_arguments, pdb_with_results, rsuffix):

    logger.info(str(command_line_arguments))

    log_differing_atoms(pdb_with_results, logger, command_line_arguments.other,
                        command_line_arguments.reference)

    logger.info("Calculated RMSD between {} and {} ".format(
        command_line_arguments.reference, command_line_arguments.other))

    squared_distance_datafield = 'dr2'+rsuffix

    worst_fit_record = pdb_with_results.loc[
        pdb_with_results[squared_distance_datafield].idxmax()]
    logger.info(
        "Worst fitting atom : chain {} residue {} atomname {} distance {:5.3} Å ".
        format(worst_fit_record['chain_id'],
               worst_fit_record['residue_number'], worst_fit_record['atom_name'],
               np.sqrt(worst_fit_record[squared_distance_datafield])))

    best_fit_record = pdb_with_results.loc[pdb_with_results[squared_distance_datafield].idxmin(
    )]
    logger.info(
        "Best fitting atom : chain {} residue {} atomname {} distance {:5.3} Å ".
        format(best_fit_record['chain_id'], best_fit_record['residue_number'],
               best_fit_record['atom_name'], np.sqrt(best_fit_record[squared_distance_datafield])))

    logger.info("\tTotal RMSD : {:5.3} Å".format(
        np.sqrt(pdb_with_results[squared_distance_datafield].mean())))

    logger.info("--------\nPer-atom distance distribution")
    logger.info(pdb_with_results[squared_distance_datafield].apply(
        lambda x: np.sqrt(x)).describe())
    logger.info("--------")

    logger.info("--------\nPer-residue RMSDs distribution: \n")
    logger.info(pdb_with_results['b_factor'].describe())
    logger.info("--------")

    logger.info("Wrote result to {} .".format(command_line_arguments.output))

    return


def write_output_pdb(command_line_arguments, pdb_with_results):

    # take the reference pdb file as template for the output
    ouput_pdb = PandasPdb().read_pdb(command_line_arguments.other)

    pdb_with_results.dropna(subset=['atom_number'], inplace=True)
    pdb_with_results.dropna(subset=['b_factor'], inplace=True)

    pdb_with_results['atom_number'] = pd.to_numeric(
        pdb_with_results['atom_number'], downcast='integer')
    # Drop non-pdb columns (all *_ref and *_merge columns) to suppress warnings when writing
    ouput_pdb.df['ATOM'] = pdb_with_results.drop(
        [key for key in pdb_with_results.keys() if ('_ref' in key)
         or ('_merge' in key)],
        axis=1)

    ouput_pdb.to_pdb(command_line_arguments.output, records=['ATOM', 'HETATM'])


command_line_arguments = get_command_line_arguments()

rsuffix = '_ref'

merged_input_pdb = merged_input_pdb_without_hydrogens(command_line_arguments.reference,
                                                      command_line_arguments.other,
                                                      rsuffix)

merged_input_pdb['chain_id'] = merged_input_pdb['chain_id'].fillna('A')
merged_input_pdb['atom_name'] = merged_input_pdb['atom_name'].fillna('X')

pdb_with_results = per_residue_rmsd_from_merged_pdb(merged_input_pdb, rsuffix)

logger = setup_logging()
log_results(logger, command_line_arguments, pdb_with_results, rsuffix)

write_output_pdb(command_line_arguments, pdb_with_results)
