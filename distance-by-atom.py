#!/usr/bin/python3
import numpy as np
import argparse
import logging
import pandas as pd

# We need biopandas here

try:
    from biopandas.pdb import PandasPdb
except:
    print("Missing biopandas, install with " "pip3 install biopandas" "")
    exit

def get_command_line_arguments():
    """ build, parse and return command line arguments """

    parser = argparse.ArgumentParser(
        description=
        "Calculate the distance between same in atoms in two pdb files and save "
        "them in an output pdb file. Aoms are matched on chain identifier, "
        "residue number and atom name (in that order), ignoring hydrogen atoms. "
        "Atoms that are not matching are reported in a log file.\n"
        "Note that coordinates are not aligned.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    requiredNamed = parser.add_argument_group('required')

    requiredNamed.add_argument(
        "--reference",
        default="reference.pdb",
        help="pdb file that provides the reference.",
        required=True)
    requiredNamed.add_argument(
        "--other",
        default="other.pdb",
        help="pdb file to compare to and template for the output pdb.",
        required=True)

    parser.add_argument("--output",
                        default="distance-by-atom.pdb",
                        help="New pdb file containing coordinates from --other and rmsd in the b-factor column.")
    parser.add_argument("--log",
                        default="distance-by-atom-mismatches.log",
                        help="Logfile with mismatching atoms in input pdbs.")

    return parser.parse_args()


def read_pdb_without_hydrogens(filename):
    pdb = PandasPdb().read_pdb(filename)
    return pdb.get('hydrogen', invert=True)


def merged_input_pdb_without_hydrogens(reference_filename, other_filename,
                                       rsuffix):
    """ 
    Combine the two pdbs into one, matching the identifying columns.
    Ignore non-matching atoms
    """

    reference_atoms = read_pdb_without_hydrogens(reference_filename)
    other_atoms = read_pdb_without_hydrogens(other_filename)

    identifying_columns = ['chain_id', 'residue_number', 'atom_name']

    return other_atoms.merge(reference_atoms.set_index(identifying_columns),
                             on=identifying_columns,
                             suffixes=[None, rsuffix],
                             how='outer',
                             indicator=True)


def log_differing_atoms(combined, logger, lhs_filename, rhs_filename):
    """ 
    Report mismatching atom names by printing mismatching columns in merged
    record 
    """

    select_only_columns = combined[combined['_merge'] != 'both'].sort_values(
        ["chain_id", "residue_number", "_merge", "atom_name"])
    select_only_columns.replace(np.nan, "", inplace=True)
    if len(select_only_columns) > 0:
        logger.warning(
            "The following atoms were skipped because they had no match in the other file\n\n \t\tleft : {} \t right : {}\n"
            .format(lhs_filename, rhs_filename))
        logger.warning(select_only_columns[[
            "chain_id", "residue_number", "residue_name", "residue_name_ref",
            "atom_name"
        ]].to_string(index=False))
        logger.warning(
            "Skipped {} atoms in total due to mismatch. Have a look in the .log file for more information."
            .format(len(select_only_columns)))

    return


def setup_logging():
    logfile_handle = logging.FileHandler("distance-by-atom.log")
    logfile_handle.setLevel(logging.INFO)

    logstream_handle = logging.StreamHandler()
    logstream_handle.setLevel(logging.WARNING)

    logging.basicConfig(level=logging.INFO,
                        format='%(message)s',
                        handlers=[logfile_handle, logstream_handle])
    return logging.getLogger('distance-by-atom')


def per_atom_distance_from_merged_pdb(pdb_data_frame, rsuffix):

    pdb_data_frame['b_factor'] = np.sqrt(
        np.square(pdb_data_frame['x_coord'] - pdb_data_frame['x_coord' + rsuffix]) 
        + np.square(pdb_data_frame['y_coord'] - pdb_data_frame['y_coord' + rsuffix]) + np.square(
                pdb_data_frame['z_coord'] - pdb_data_frame['z_coord' + rsuffix]))

    return pdb_data_frame

def log_results(logger, command_line_arguments, pdb_with_results, rsuffix):

    logger.info(str(command_line_arguments))

    log_differing_atoms(pdb_with_results, logger, command_line_arguments.other,
                        command_line_arguments.reference)

    logger.info("Calculated RMSD between {} and {} ".format(
        command_line_arguments.reference, command_line_arguments.other))

    distance_datafield = 'b_factor'

    worst_fit_record = pdb_with_results.loc[
        pdb_with_results[distance_datafield].idxmax()]
    logger.info(
        "Worst fitting atom : chain {} residue {} atomname {} distance {:5.3} Å ".
        format(worst_fit_record['chain_id'],
            worst_fit_record['residue_number'], worst_fit_record['atom_name'],
            worst_fit_record[distance_datafield]))

    best_fit_record = pdb_with_results.loc[pdb_with_results[distance_datafield].idxmin()]
    logger.info(
        "Best fitting atom : chain {} residue {} atomname {} distance {:5.3} Å ".
        format(best_fit_record['chain_id'], best_fit_record['residue_number'],
            best_fit_record['atom_name'], best_fit_record[distance_datafield]))

    logger.info("\tTotal RMSD : {:5.3} Å".format(pdb_with_results[distance_datafield].mean()))

    logger.info("--------\nPer-atom distance distribution")
    logger.info(pdb_with_results[distance_datafield].describe())
    logger.info("--------")

    logger.info("--------\nPer-residue RMSDs distribution: \n")
    logger.info(pdb_with_results['b_factor'].describe())
    logger.info("--------")

    logger.info("Wrote result to {} .".format(command_line_arguments.output))

    return


def write_output_pdb(command_line_arguments, pdb_with_results):

    # take the reference pdb file as template for the output
    ouput_pdb = PandasPdb().read_pdb(command_line_arguments.other)

    pdb_with_results.dropna(subset=['atom_number'], inplace=True)
    pdb_with_results.dropna(subset=['b_factor'], inplace=True)

    pdb_with_results['atom_number'] = pd.to_numeric(pdb_with_results['atom_number'], downcast='integer')
    # Drop non-pdb columns (all *_ref and *_merge columns) to suppress warnings when writing
    ouput_pdb.df['ATOM'] = pdb_with_results.drop(
        [key for key in pdb_with_results.keys() if ('_ref' in key) or ('_merge' in key)],
        axis=1)

    ouput_pdb.to_pdb(command_line_arguments.output, records=['ATOM', 'HETATM'])


command_line_arguments = get_command_line_arguments()

rsuffix = '_ref'

merged_input_pdb = merged_input_pdb_without_hydrogens(command_line_arguments.reference,
                                              command_line_arguments.other,
                                              rsuffix)

pdb_with_results = per_atom_distance_from_merged_pdb(merged_input_pdb, rsuffix)

logger = setup_logging()
log_results(logger, command_line_arguments, pdb_with_results, rsuffix)

write_output_pdb(command_line_arguments, pdb_with_results)
